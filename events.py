import logging, hangups, plugins, asyncio, pytz

logger = logging.getLogger(__name__)

from urllib.request import urlopen
from icalendar import Calendar, Events

def _initialise(bot):
  plugins.register_user_command(["events"])


@asyncio.coroutine
def events(bot, event, *args):
  """Get events for the next week. Only works in authorised hangouts"""
  
  try:
    calurl = bot.get_config_option("calendar_url")
  except:
    logger.error("Something went wrong getting the calendar URL.")
    return

  try:
    tzoffset = bot.get_config_option("timezone")
  except:
    tzoffset = "UTC"

  gcal = Calendar.from_ical(urlopen(calurl).read())
  
  tz = pytz.timezone(tzoffset)

  gevents = ""
  for event in gcal.subcomponents:
    if event.decoded("DTEND") > datetime.utcnow().replace(tzinfo=pytz.utc):
      gevents = gevents + "<b>" + event["SUMMARY"] + "</b>: " + str(event.decoded("DTSTART").replace(tzinfo=pytz.utc).astimezone(tz)) + " - " + event["URL"] + "<br>"
  
  yield from bot.coro_send_message(event.conv, gevents)


